# Beeline cloud test task

Test task for beeline cloud

NOTE: for some reason swagger does not run requests from doc. Added postman collection for testing

Задание:

Необходимо написать веб сервис который занимается хранением профилей пользователей и их авторизацией.

Профиль имеет набор полей:
1. id (uuid, unique)
2. email
3. username (unique)
4. password
5. admin (bool)

У сервиса должeн быть набор ручек(rest, json):
- /user (создвние пользователя, выдача листинга пользователей)
- /user/{id} (выдача профиля по id, изменение и удаление профиля)
Сервис использует basic access authentication (https://en.wikipedia.org/wiki/Basic_access_authentication)

Просмотр профилей могут просматривать все зарегистрированные пользователи, создавать, изменять и удалять только с пометкой admin.

Для хранения данных профилей необходимо реализовать примитивную in memory базу данных