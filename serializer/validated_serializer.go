package serializer

import (
	"github.com/go-playground/validator/v10"
	"github.com/json-iterator/go"
)

type ValidatedSerializer struct {
	validator *validator.Validate
}

func NewValidatedSerializer() ValidatedSerializer {
	return ValidatedSerializer{
		validator: validator.New(validator.WithRequiredStructEnabled()),
	}
}

func (s ValidatedSerializer) Marshal(v interface{}) ([]byte, error) {
	return jsoniter.Marshal(v)
}

func (s ValidatedSerializer) Unmarshal(data []byte, v interface{}) error {
	err := jsoniter.Unmarshal(data, v)
	if err != nil {
		return err
	}
	return s.validator.Struct(v)
}
