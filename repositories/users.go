package repositories

import (
	"github.com/google/uuid"
	"sync"
	"testTaskBeeline/entities"
)

type userInMemoryRepository struct {
	mu        sync.Mutex
	users     map[uuid.UUID]entities.User
	usernames map[string]uuid.UUID
}

func NewUserInMemoryRepository() UserRepository {
	return &userInMemoryRepository{
		users:     make(map[uuid.UUID]entities.User),
		usernames: make(map[string]uuid.UUID),
	}
}

func (r *userInMemoryRepository) Create(user entities.User) (*entities.User, error) {
	r.mu.Lock()
	defer r.mu.Unlock()

	if user.Id == uuid.Nil {
		user.Id = uuid.New()
	}

	if _, ok := r.users[user.Id]; ok {
		return nil, alreadyExistsUserRepoError{id: user.Id.String()}
	}
	if _, ok := r.usernames[user.Username]; ok {
		return nil, notUniqueUsernameUserRepoError{username: user.Username}
	}

	r.users[user.Id] = user
	r.usernames[user.Username] = user.Id

	return &user, nil
}

func (r *userInMemoryRepository) GetByUuid(id uuid.UUID) *entities.User {
	r.mu.Lock()
	defer r.mu.Unlock()

	u, ok := r.users[id]
	if !ok {
		return nil
	}

	return &u
}

func (r *userInMemoryRepository) GetByUsername(username string) *entities.User {
	r.mu.Lock()
	defer r.mu.Unlock()

	id, ok := r.usernames[username]
	if !ok {
		return nil
	}
	u, ok := r.users[id]
	if !ok {
		return nil
	}

	return &u
}

func (r *userInMemoryRepository) GetAll() []entities.User {
	values := make([]entities.User, 0, len(r.users))

	for _, value := range r.users {
		values = append(values, value)
	}

	return values
}

func (r *userInMemoryRepository) Update(userUpdate entities.UserUpdate, uuid uuid.UUID) (*entities.User, error) {
	r.mu.Lock()
	defer r.mu.Unlock()

	user, ok := r.users[uuid]
	if !ok {
		return nil, doesNotExistUserRepoError{id: uuid.String()}
	}
	if _, ok = r.usernames[user.Username]; ok && userUpdate.Username != user.Username {
		return nil, notUniqueUsernameUserRepoError{username: user.Username}
	}

	user.Admin = userUpdate.Admin
	user.Username = userUpdate.Username
	user.Password = userUpdate.Password
	user.Email = userUpdate.Email

	r.users[uuid] = user
	r.usernames[user.Username] = user.Id

	return &user, nil
}

func (r *userInMemoryRepository) Delete(id uuid.UUID) *entities.User {
	r.mu.Lock()
	defer r.mu.Unlock()

	u, ok := r.users[id]
	if !ok {
		return nil
	}
	delete(r.users, id)
	delete(r.usernames, u.Username)

	return &u
}
