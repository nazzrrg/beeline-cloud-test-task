package repositories

import (
	"github.com/google/uuid"
	"testTaskBeeline/entities"
)

type UserRepository interface {
	Create(user entities.User) (*entities.User, error)
	GetByUuid(id uuid.UUID) *entities.User
	GetByUsername(username string) *entities.User
	GetAll() []entities.User
	Update(updateUser entities.UserUpdate, uuid uuid.UUID) (*entities.User, error)
	Delete(id uuid.UUID) *entities.User
}
