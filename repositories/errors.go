package repositories

import "fmt"

type alreadyExistsUserRepoError struct {
	id string
}

func (e alreadyExistsUserRepoError) Error() string {
	return fmt.Sprintf("user with id %s already exists", e.id)
}

type notUniqueUsernameUserRepoError struct {
	username string
}

func (e notUniqueUsernameUserRepoError) Error() string {
	return fmt.Sprintf("%s is not a unique value", e.username)
}

type doesNotExistUserRepoError struct {
	id string
}

func (e doesNotExistUserRepoError) Error() string {
	return fmt.Sprintf("user with id %s does not exist", e.id)
}
