package entities

import "github.com/google/uuid"

// User model info
// @Description User information
type User struct {
	Id       uuid.UUID `json:"id" example:"00000000-0000-0000-0000-000000000002"`
	Email    string    `json:"email" validate:"required,email" example:"user@user.com"`
	Username string    `json:"username" validate:"required,alphanum" example:"user"`
	Password string    `json:"password" validate:"required" example:"pass"`
	Admin    bool      `json:"admin" example:"false"`
}

// UserUpdate model info
// @Description User information for update request
type UserUpdate struct {
	Email    string `json:"email" validate:"required,email" example:"user@user.com"`
	Username string `json:"username" validate:"required,alphanum" example:"user"`
	Password string `json:"password" validate:"required" example:"pass"`
	Admin    bool   `json:"admin" example:"false"`
}
