package http_handlers

import (
	"github.com/valyala/fasthttp"
	"testTaskBeeline/repositories"
	"testTaskBeeline/serializer"
)

// GetUsersHandler godoc
//
//	@Summary	Get all users
//	@Tags		users
//	@Produce	json
//	@Produce	plain
//	@Security	BasicAuth
//	@Success	200	{array}		entities.User	"All users"
//	@Failure	401	{string}	string			"Error description"
//	@Failure	500	{string}	string			"Error description"
//	@Router		/user [get]
func GetUsersHandler(serializer serializer.ValidatedSerializer, repo repositories.UserRepository) func(ctx *fasthttp.RequestCtx) {
	return func(ctx *fasthttp.RequestCtx) {
		bytes, err := serializer.Marshal(repo.GetAll())
		if err != nil {
			ctx.Error(fasthttp.StatusMessage(fasthttp.StatusInternalServerError), fasthttp.StatusInternalServerError)
			return
		}
		ctx.Response.Header.SetContentType("application/json")
		ctx.Response.SetBody(bytes)
	}
}
