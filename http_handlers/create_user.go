package http_handlers

import (
	"github.com/valyala/fasthttp"
	"testTaskBeeline/entities"
	"testTaskBeeline/repositories"
	"testTaskBeeline/serializer"
)

// CreateUserHandler godoc
//
//	@Summary	Create user
//	@Tags		users
//	@Accept		json
//	@Produce	json
//	@Produce	plain
//	@Param		user	body	entities.User	true	"New user"
//	@Security	BasicAuth
//	@Success	200	{object}	entities.User	"Created user"
//	@Failure	400	{string}	string			"Error description"
//	@Failure	401	{string}	string			"Error description"
//	@Failure	500	{string}	string			"Error description"
//	@Router		/user [post]
func CreateUserHandler(serializer serializer.ValidatedSerializer, repo repositories.UserRepository) func(ctx *fasthttp.RequestCtx) {
	return func(ctx *fasthttp.RequestCtx) {
		user := entities.User{}
		err := serializer.Unmarshal(ctx.PostBody(), &user)
		if err != nil {
			ctx.Error(fasthttp.StatusMessage(fasthttp.StatusBadRequest), fasthttp.StatusBadRequest)
			return
		}

		newUser, err := repo.Create(user)
		if err != nil {
			ctx.Error(err.Error(), fasthttp.StatusBadRequest)
			return
		}

		bytes, err := serializer.Marshal(newUser)
		if err != nil {
			ctx.Error(fasthttp.StatusMessage(fasthttp.StatusInternalServerError), fasthttp.StatusInternalServerError)
			return
		}
		ctx.Response.Header.SetContentType("application/json")
		ctx.Response.SetBody(bytes)
	}
}
