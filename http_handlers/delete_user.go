package http_handlers

import (
	"github.com/google/uuid"
	"github.com/valyala/fasthttp"
	"testTaskBeeline/repositories"
	"testTaskBeeline/serializer"
)

// DeleteUserHandler godoc
//
//	@Summary	Delete user
//	@Tags		users
//	@Produce	json
//	@Produce	plain
//	@Param		id	path	string	true	"User ID"
//	@Security	BasicAuth
//	@Success	200	{object}	entities.User	"Deleted user"
//	@Failure	400	{string}	string			"Error description"
//	@Failure	401	{string}	string			"Error description"
//	@Failure	404	{string}	string			"Error description"
//	@Failure	500	{string}	string			"Error description"
//	@Router		/user/{id} [delete]
func DeleteUserHandler(serializer serializer.ValidatedSerializer, repo repositories.UserRepository) func(ctx *fasthttp.RequestCtx) {
	return func(ctx *fasthttp.RequestCtx) {
		ifId := ctx.UserValue("userId")
		if ifId == nil {
			ctx.Error(fasthttp.StatusMessage(fasthttp.StatusBadRequest), fasthttp.StatusBadRequest)
			return
		}

		strId := ifId.(string) // guaranteed to be string by fasthttprouter
		uuidId, err := uuid.Parse(strId)
		if err != nil {
			// send nil as return for non-uuid. do not disclose uuid requirement as bad request
			ctx.Error(fasthttp.StatusMessage(fasthttp.StatusNotFound), fasthttp.StatusNotFound)
			return
		}

		deletedUser := repo.Delete(uuidId)
		if deletedUser == nil {
			ctx.Error(fasthttp.StatusMessage(fasthttp.StatusNotFound), fasthttp.StatusNotFound)
			return
		}

		bytes, err := serializer.Marshal(deletedUser)
		if err != nil {
			ctx.Error(fasthttp.StatusMessage(fasthttp.StatusInternalServerError), fasthttp.StatusInternalServerError)
			return
		}
		ctx.Response.Header.SetContentType("application/json")
		ctx.Response.SetBody(bytes)
	}
}
