package http_handlers

import (
	"github.com/google/uuid"
	"github.com/valyala/fasthttp"
	"testTaskBeeline/entities"
	"testTaskBeeline/repositories"
	"testTaskBeeline/serializer"
)

// UpdateUserHandler godoc
//
//	@Summary	Update user
//	@Tags		users
//	@Accept		json
//	@Produce	json
//	@Produce	plain
//	@Param		id		path	string				true	"User ID"
//	@Param		user	body	entities.UserUpdate	true	"User update model"
//	@Security	BasicAuth
//	@Success	200	{object}	entities.User	"Updated user"
//	@Failure	400	{string}	string			"Error description"
//	@Failure	401	{string}	string			"Error description"
//	@Failure	404	{string}	string			"Error description"
//	@Failure	500	{string}	string			"Error description"
//	@Router		/user/{id} [put]
func UpdateUserHandler(serializer serializer.ValidatedSerializer, repo repositories.UserRepository) func(ctx *fasthttp.RequestCtx) {
	return func(ctx *fasthttp.RequestCtx) {
		ifId := ctx.UserValue("userId")
		if ifId == nil {
			ctx.Error(fasthttp.StatusMessage(fasthttp.StatusBadRequest), fasthttp.StatusBadRequest)
			return
		}

		strId := ifId.(string) // guaranteed to be string by fasthttprouter
		uuidId, err := uuid.Parse(strId)
		if err != nil {
			// send nil as return for non-uuid. do not disclose uuid requirement as bad request
			ctx.Error(fasthttp.StatusMessage(fasthttp.StatusNotFound), fasthttp.StatusNotFound)
			return
		}

		user := entities.UserUpdate{}
		err = serializer.Unmarshal(ctx.PostBody(), &user)
		if err != nil {
			ctx.Error(fasthttp.StatusMessage(fasthttp.StatusBadRequest), fasthttp.StatusBadRequest)
			return
		}

		updatedUser, err := repo.Update(user, uuidId)
		if err != nil {
			ctx.Error(err.Error(), fasthttp.StatusBadRequest)
			return
		}

		bytes, err := serializer.Marshal(updatedUser)
		if err != nil {
			ctx.Error(fasthttp.StatusMessage(fasthttp.StatusInternalServerError), fasthttp.StatusInternalServerError)
			return
		}
		ctx.Response.Header.SetContentType("application/json")
		ctx.Response.SetBody(bytes)
	}
}
