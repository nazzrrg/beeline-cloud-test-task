package wrappers

import (
	"github.com/valyala/fasthttp"
	"testTaskBeeline/entities"
)

func WrapAdmin(handler fasthttp.RequestHandler) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		user := ctx.UserValue(AuthenticatedUserKey)
		if user == nil {
			ctx.Error(fasthttp.StatusMessage(fasthttp.StatusUnauthorized), fasthttp.StatusUnauthorized)
			return
		}
		if !user.(*entities.User).Admin {
			ctx.Error(fasthttp.StatusMessage(fasthttp.StatusUnauthorized), fasthttp.StatusUnauthorized)
			return
		}
		handler(ctx)
	}
}
