package wrappers

import (
	"bytes"
	"encoding/base64"
	"github.com/valyala/fasthttp"
	"testTaskBeeline/repositories"
)

var basicAuthPrefix = []byte("Basic ")

var AuthenticatedUserKey = struct{}{}

func WrapAuth(handler fasthttp.RequestHandler, repo repositories.UserRepository) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		auth := ctx.Request.Header.Peek("Authorization")
		if !bytes.HasPrefix(auth, basicAuthPrefix) {
			ctx.Response.Header.Set("WWW-Authenticate", "Basic realm=Restricted")
			ctx.Error(fasthttp.StatusMessage(fasthttp.StatusUnauthorized), fasthttp.StatusUnauthorized)
			return
		}

		payload, err := base64.StdEncoding.DecodeString(string(auth[len(basicAuthPrefix):]))
		if err != nil {
			ctx.Error(fasthttp.StatusMessage(fasthttp.StatusInternalServerError), fasthttp.StatusInternalServerError)
			return
		}

		parts := bytes.SplitN(payload, []byte(":"), 2)
		if len(parts) == 1 {
			ctx.Response.Header.Set("WWW-Authenticate", "Basic realm=Restricted")
			ctx.Error(fasthttp.StatusMessage(fasthttp.StatusUnauthorized), fasthttp.StatusUnauthorized)
			return
		}
		username, pass := string(parts[0]), string(parts[1])

		user := repo.GetByUsername(username)
		if user == nil {
			ctx.Error(fasthttp.StatusMessage(fasthttp.StatusUnauthorized), fasthttp.StatusUnauthorized)
			return
		}
		if user.Password != pass {
			ctx.Error(fasthttp.StatusMessage(fasthttp.StatusUnauthorized), fasthttp.StatusUnauthorized)
			return
		}

		ctx.SetUserValue(AuthenticatedUserKey, user)
		handler(ctx)
	}
}
