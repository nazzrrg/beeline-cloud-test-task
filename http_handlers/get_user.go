package http_handlers

import (
	"github.com/google/uuid"
	"github.com/valyala/fasthttp"
	"testTaskBeeline/repositories"
	"testTaskBeeline/serializer"
)

// GetUserHandler godoc
//
//	@Summary	Get user by id
//	@Tags		users
//	@Produce	json
//	@Produce	plain
//	@Param		id	path	string	true	"User ID"
//	@Security	BasicAuth
//	@Success	200	{object}	entities.User	"User"
//	@Failure	400	{string}	string			"Error description"
//	@Failure	401	{string}	string			"Error description"
//	@Failure	404	{string}	string			"Error description"
//	@Failure	500	{string}	string			"Error description"
//	@Router		/user/{id} [get]
func GetUserHandler(serializer serializer.ValidatedSerializer, repo repositories.UserRepository) func(ctx *fasthttp.RequestCtx) {
	return func(ctx *fasthttp.RequestCtx) {
		ifId := ctx.UserValue("userId")
		if ifId == nil {
			ctx.Error(fasthttp.StatusMessage(fasthttp.StatusBadRequest), fasthttp.StatusBadRequest)
			return
		}

		strId := ifId.(string) // guaranteed to be string by fasthttprouter
		uuidId, err := uuid.Parse(strId)
		if err != nil {
			// send nil as return for non-uuid. do not disclose uuid requirement as bad request
			ctx.Error(fasthttp.StatusMessage(fasthttp.StatusNotFound), fasthttp.StatusNotFound)
			return
		}

		user := repo.GetByUuid(uuidId)
		if user == nil {
			ctx.Error(fasthttp.StatusMessage(fasthttp.StatusNotFound), fasthttp.StatusNotFound)
			return
		}

		bytes, err := serializer.Marshal(user)
		if err != nil {
			ctx.Error(fasthttp.StatusMessage(fasthttp.StatusInternalServerError), fasthttp.StatusInternalServerError)
			return
		}
		ctx.Response.Header.SetContentType("application/json")
		ctx.Response.SetBody(bytes)
	}
}
