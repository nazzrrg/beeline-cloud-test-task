package main

import (
	"github.com/buaazp/fasthttprouter"
	"github.com/google/uuid"
	"github.com/swaggo/http-swagger"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/fasthttpadaptor"
	"log"
	_ "testTaskBeeline/docs"
	"testTaskBeeline/entities"
	"testTaskBeeline/http_handlers"
	"testTaskBeeline/http_handlers/wrappers"
	"testTaskBeeline/repositories"
	"testTaskBeeline/serializer"
)

//	@title			User service test task
//	@version		1.0
//	@description	This is a test task for Beeline Cloud by nazzrrg.

//	@contact.name	Nazzrrg
//	@contact.url	https://t.me/nazzrrg

//	@host		localhost:8080
//	@BasePath	/

//	@securityDefinitions.basic	BasicAuth
//	@in							header
//	@name						Authorization
//	@description				Basic auth

func main() {
	repo := repositories.NewUserInMemoryRepository()
	err := addDefaultUsers(repo)
	if err != nil {
		log.Fatalf("failed to add default users: %s", err)
		return
	}

	validatedSerializer := serializer.NewValidatedSerializer()
	router := fasthttprouter.New()
	router.GET("/user", wrappers.WrapAuth(http_handlers.GetUsersHandler(validatedSerializer, repo), repo))
	router.POST("/user", wrappers.WrapAuth(wrappers.WrapAdmin(http_handlers.CreateUserHandler(validatedSerializer, repo)), repo))
	router.GET("/user/:userId", wrappers.WrapAuth(http_handlers.GetUserHandler(validatedSerializer, repo), repo))
	router.PUT("/user/:userId", wrappers.WrapAuth(wrappers.WrapAdmin(http_handlers.UpdateUserHandler(validatedSerializer, repo)), repo))
	router.DELETE("/user/:userId", wrappers.WrapAuth(wrappers.WrapAdmin(http_handlers.DeleteUserHandler(validatedSerializer, repo)), repo))

	router.GET("/swagger/*filepath", fasthttpadaptor.NewFastHTTPHandlerFunc(httpSwagger.WrapHandler))

	err = fasthttp.ListenAndServe(":8080", router.Handler)
	if err != nil {
		log.Fatalf("failed to listen on port 8080: %s", err)
		return
	}
}

func addDefaultUsers(repo repositories.UserRepository) error {
	_, err := repo.Create(entities.User{
		Id:       uuid.MustParse("00000000-0000-0000-0000-000000000001"),
		Email:    "admin@admin.com",
		Username: "admin",
		Password: "admin",
		Admin:    true,
	})
	if err != nil {
		return err
	}
	_, err = repo.Create(entities.User{
		Id:       uuid.MustParse("00000000-0000-0000-0000-000000000002"),
		Email:    "user@user.com",
		Username: "user",
		Password: "pass",
		Admin:    false,
	})
	return err
}
